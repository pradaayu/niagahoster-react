import React, {Fragment} from 'react';
import Routes from "./routes/Routes";
import './App.css';

const App = () => {
  return(
    <Fragment className="App">
      <Routes/>
    </Fragment>
  )
}

export default App;
