import React, {Fragment} from "react";
import { Route } from "react-router-dom";
import HomePage from "../Home/HomePage"
import HeaderTop from "../layouts/HeaderTop";
import FooterBottom from "../layouts/FooterBottom";
import UnlimitedHosting from "../views/UnlimitedHosting";
import CloudHosting from "../views/CloudHosting";
import CloudVps from "../views/CloudVPS";
import DomainPage from "../views/DomainPage";
import AfiliasiPage from "../views/AfiliasiPage";
import BlogPage from "../views/BlogPage";
import LoginPage from "../views/LoginPage"


const Routes = () => {
    return (
        <Fragment>
            <HeaderTop />
            <Route path="/" component={HomePage} exact />
            <Route path="/unlimited" component={UnlimitedHosting} exact />
            <Route path="/hosting" component={CloudHosting} exact />
            <Route path="/vps" component={CloudVps} exact />
            <Route path="/domain" component={DomainPage} exact />
            <Route path="/afiliasi" component={AfiliasiPage} exact />
            <Route path="/blog" component={BlogPage} exact />
            <Route path="/login" component={LoginPage} exact />
            <FooterBottom />
        </Fragment>
    );
}
export default Routes;