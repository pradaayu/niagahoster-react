import React, {Fragment} from "react";
import {Link} from "react-router-dom";

const HeroPage = () => {
    return (
        <Fragment>
            <section className="hero hero--power-up-website">
               <div className="container">
                   <div className="row">
                       <div className="col-lg-5">
                           <h1 className="hero__title hero__title--ramadhan">Unlimited Web Hosting Terbaik di Indonesia </h1>
                            <h2 className="hero__description hero__description--power-up-website">Ada banyak peluang bisa Anda raih dari rumah dengan memiliki website. Manfaatkan diskon hosting hingga 75% dan tetap produktif di bulan Ramadhan bersama Niagahoster.</h2>
                            <div className="countdown">
                                <p className="countdown__title">Yuk segera order karena diskon dapat berakhir sewaktu-waktu!</p>
                                <ul className="countdown__counter  d-flex">
                                    <li className="countdown__counter-item">
                                        <p id="days" className="countdown__counter-item-time">0</p>
                                        <p className="countdown__counter-item-title">Hari</p>
                                    </li>
                                    <li className="countdown__counter-item">
                                        <p id="hours" className="countdown__counter-item-time">4</p>
                                        <p className="countdown__counter-item-title">Jam</p>
                                    </li>
                                    <li className="countdown__counter-item">
                                        <p id="minutes" className="countdown__counter-item-time">22</p>
                                        <p className="countdown__counter-item-title">Menit</p>
                                    </li>
                                    <li className="countdown__counter-item">
                                        <p id="seconds" className="countdown__counter-item-time">37</p>
                                        <p className="countdown__counter-item-title">Detik</p>
                                    </li>
                                </ul>
                            </div>
                            <Link className="nh-btn nh-btn--orange" to="">Pilih Sekarang</Link>
                        </div>
                        <div className="hero__image-wrapper hero__image-wrapper--power-up-website col-lg-7">
                            <img className="hero__image hero__image--ramadhan img-fluid" src={require("../../assets/images/hero-home-ramadhan.webp")} alt=""/>
                        </div>
                    </div>
                </div>
            </section>
        </Fragment>
    )
}
export default HeroPage;

