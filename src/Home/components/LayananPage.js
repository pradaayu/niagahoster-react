import React from "react";
import {Link} from "react-router-dom";

const LayananPage = (props) => {
    let lists = props.dataLayanan.map(item =>
        <li className="our-services-v2__card">
            <Link to="https://www.niagahoster.co.id/hosting-indonesia">
            <img className="our-services-v2__service-image img-fluid" src={item.image} alt={item.name}/>
            <h4 className="">{item.name}</h4>
            <p>{item.description}</p>
            <p>{item.tag}</p>
            <p>{item.price}</p>
            </Link>            
        </li>
    )
    return(
        <section className="our-services-v2">
            <div className="container">
                <h3 className="our-services-v2__title">Layanan Niagahoster</h3>
                <ul className="our-services-v2__list">
                    {lists}
                </ul>
                <div className="our-services-website row justify-content-center">
                    <div className="col-lg-6">
                        <div className="card">
                            <div className="card-body our-services-website__link">
                                <div className="row align-items-center">
                                    <div className="col-lg-3 col-12">
                                        <img className="our-services-website__img" src={require("../../assets/images/home-pembuatan-website.svg")} alt="pembuatan website"/>
                                    </div>
                                    <div className="col-lg-9">
                                        <h3 className="our-services-website__title">Pembuatan Website</h3>
                                        <p className="our-services-website__description">500 perusahaan lebih percayakan pembuatan websitenya pada kami. <Link to="https://www.niagahoster.co.id/membuat-website" title="Cek Selengkapnya" target="_blank">Cek selengkapnya...</Link></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default LayananPage;