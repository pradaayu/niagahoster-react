import React, {Fragment} from "react";
import { Link } from "react-router-dom";
import "../assets/css/style.css";
import "../assets/css/responsive.css";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {
    faPhone,
    faCommentAlt,
    faShoppingCart
}from "@fortawesome/free-solid-svg-icons";

const HeaderTop = () => {
    return (
       <Fragment>
           <section className="nh-navbar">
               <div className="container">
                    <ul className="nh-navbar__subnav d-none d-xl-flex">
                        <li className="nh-navbar__subnav-item">
                            <Link to href="tel:0274-2885822" className="nh-navbar__subnav-item-link" >
                                <FontAwesomeIcon icon={faPhone} className="nh-navbar__subnav-item-icon" />
                                0274-2885822
                            </Link>
                        </li>
                        <li className="nh-navbar__subnav-item">
                            <Link to="#" className="nh-navbar__subnav-item-link">
                                <FontAwesomeIcon icon={faCommentAlt} className="nh-navbar__subnav-item-icon" />
                                Live Chat
                            </Link>
                        </li>
                        <li className="nh-navbar__subnav-item">
                            <Link to="https://panel.niagahoster.co.id/cart" className="nh-navbar__subnav-item-link">
                                <FontAwesomeIcon icon={faShoppingCart} className="nh-navbar__subnav-item-icon" />
                            </Link>
                        </li>
                    </ul>
                    <nav className="nh-navbar__nav navbar navbar-expand-xl bg-transparent">
                        <Link to="/" className="navbar-brand">
                            <img className="img-fluid d-none d-md-block" src={require("../assets/images/nh-logo.svg")} alt="logo"/> 
                        </Link>
                        <div className="collapse navbar-collapse">
                            <ul className="navbar-nav ml-auto">
                                <li className="nh-navbar__nav-item">
                                    <Link  className="nh-navbar__nav-link  nav-link" to="/unlimited">UNLIMITED HOSTING</Link>
                                </li>
                                <li className="nh-navbar__nav-item">
                                        <Link className="nh-navbar__nav-link  nav-link" to="/hosting">CLOUD HOSTING</Link>
                                </li>
                                <li className="nh-navbar__nav-item">
                                    <Link className="nh-navbar__nav-link  nav-link" to="/vps">CLOUD VPS</Link>
                                </li>
                                <li className="nh-navbar__nav-item">
                                    <Link className="nh-navbar__nav-link  nav-link" to="/domain">DOMAIN</Link>
                                </li>
                                <li className="nh-navbar__nav-item">
                                    <Link className="nh-navbar__nav-link  nav-link" to="/afiliasi">AFILIASI</Link>
                                </li>
                                <li className="nh-navbar__nav-item">
                                    <Link className="nh-navbar__nav-link  nav-link" to="/blog">BLOG</Link>
                                </li>
                                <li className="nh-navbar__nav-item d-none d-xl-block login-btn-xl">
                                    <Link className="nh-btn nh-btn--transparent nh-btn--color-white nh-btn--border-white nh-btn--radius-50" to="/login">LOGIN</Link>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
           </section>
        </Fragment>
             
    )

}
export default HeaderTop;