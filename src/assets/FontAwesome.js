// npm uninstall <name> --save to uninstall and removes it from dependencies
//npm uninstall -g <name> --save to remove it globally
//import library
import {library} from "@fortawesome/fontawesome-svg-core";
//import icons
import {
    faFacebook, 
    faTwitter, 
    faInstagram, 
    faLinkedin
} from "@fortawesome/free-brands-svg-icons";
import {
    faPhone,
    faComment,
    faShoppingCart
}from "@fortawesome/free-solid-svg-icons";

library.add(
    faFacebook,
    faTwitter,
    faInstagram,
    faLinkedin,
    faPhone,
    faComment,
    faShoppingCart
);

